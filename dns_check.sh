#! /bin/bash

#cat /etc/resolv.conf

while read line; do
  section=$(echo $line | cut -d' ' -f1)
  if [[ "$section" == "nameserver" ]]; then
    ip_address=$(echo $line | cut -d' ' -f2)
  fi
done < /etc/resolv.conf


nmap -sU -p 53 "$ip_address"
